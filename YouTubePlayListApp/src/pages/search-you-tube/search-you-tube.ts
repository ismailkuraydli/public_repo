import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import {YoutubeProvider} from './../../providers/youtube/youtube'
import { Observable } from 'rxjs/Observable';
import { MenuController } from 'ionic-angular/components/app/menu-controller';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { PlaylistsListComponent } from '../../components/playlists-list/playlists-list';

@IonicPage()
@Component({
  selector: 'page-search-you-tube',
  templateUrl: 'search-you-tube.html',
})
export class SearchYouTubePage {

  @ViewChild('searchWord') searchWord;
  videos: Observable<any[]>;

  constructor(
    public navCtrl: NavController, 
    public menu: MenuController, 
    public navParams: NavParams, 
    private youtube: YoutubeProvider, 
    private alertCtrl: AlertController,
    public modalCtrl: ModalController
  ) {
  }
  /**
   * searches youtube videos by word typed in input field
   */
  searchYoutube() {
    this.videos = this.youtube.getSearchResults(this.searchWord._value);
    this.videos.subscribe(data => {
      console.log('videos: ', data);
    }, err => {
      let alert = this.alertCtrl.create({
        title: 'Error',
        message: 'No Videos matched your search',
        buttons:['OK']
      });
      alert.present();
    })
  }
  /**
   * Opens Modal to select playlist where video will be added
   * @param video 
   */
  addVideoToPlaylists(video) {
    let playlistListModal = this.modalCtrl.create(PlaylistsListComponent, {video: video});
    playlistListModal.present();
  }
}
