import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SearchYouTubePage } from './search-you-tube';

@NgModule({
  declarations: [
    SearchYouTubePage,
  ],
  imports: [
    IonicPageModule.forChild(SearchYouTubePage),
  ],
})
export class SearchYouTubePageModule {}
