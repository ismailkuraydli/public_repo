import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PlaylistStorageProvider } from '../../providers/playlist-storage/playlist-storage';
import { Events } from 'ionic-angular/util/events';


@IonicPage()
@Component({
  selector: 'page-singleplaylist',
  templateUrl: 'singleplaylist.html',
})

export class SingleplaylistPage {

  videos: Promise<any[]>;
  playlistName: string;
  playlistIndex: any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public pListStorage: PlaylistStorageProvider,
    public events: Events
  ) {
    this.playlistIndex = this.navParams.get('index');
    this.videos = this.pListStorage.getPlaylistVideos(this.playlistIndex);
    this.playlistName = this.navParams.get('name');
    this.events.subscribe('storage:changed',(result) => {
      this.videos = this.pListStorage.getPlaylistVideos(this.playlistIndex);
    })
  }
  /**
   * Delets selected video by index from selected playlist
   * @param videoIndex 
   */
  deleteVideo(videoIndex) {
    this.pListStorage.deleteVideoFromPlaylist(this.playlistIndex, videoIndex);
  }
}
