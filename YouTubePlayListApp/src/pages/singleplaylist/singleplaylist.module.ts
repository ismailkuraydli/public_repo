import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SingleplaylistPage } from './singleplaylist';

@NgModule({
  declarations: [
    SingleplaylistPage,
  ],
  imports: [
    IonicPageModule.forChild(SingleplaylistPage),
  ],
})
export class SingleplaylistPageModule {}
