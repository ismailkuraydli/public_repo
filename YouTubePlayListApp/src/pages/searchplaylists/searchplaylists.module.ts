import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SearchplaylistsPage } from './searchplaylists';

@NgModule({
  declarations: [
    SearchplaylistsPage,
  ],
  imports: [
    IonicPageModule.forChild(SearchplaylistsPage),
  ],
})
export class SearchplaylistsPageModule {}
