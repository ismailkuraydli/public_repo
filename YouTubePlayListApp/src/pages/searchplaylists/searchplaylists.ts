import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';



@IonicPage()
@Component({
  selector: 'page-searchplaylists',
  templateUrl: 'searchplaylists.html',
})
export class SearchplaylistsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  
}
