import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PlaylistStorageProvider } from '../../providers/playlist-storage/playlist-storage';
import { SingleplaylistPage } from '../singleplaylist/singleplaylist';
import { Events } from 'ionic-angular/util/events';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { EditPlaylistComponent } from '../../components/edit-playlist/edit-playlist';

@IonicPage()
@Component({
  selector: 'page-playlists',
  templateUrl: 'playlists.html',
})
export class PlaylistsPage {

  playlists: Promise<any[]>;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public pListStorage: PlaylistStorageProvider,
    public events: Events,
    public modalCtrl: ModalController
  ) {
    this.playlists = this.pListStorage.getPlaylists();
    this.events.subscribe('storage:changed',(result) => {
      // this.navCtrl.setRoot(PlaylistsPage);
      this.playlists = this.pListStorage.getPlaylists();
    })
  }
/**
 * Navigates to single playlist page where videos are displayed 
 * @param playListIndex 
 * @param playlistName 
 */
  openPlaylist(playListIndex, playlistName) {
    this.navCtrl.push(SingleplaylistPage, {index: playListIndex, name: playlistName});
  }
  /**
   * Deletes selected playlist by index
   * @param playListIndex 
   */
  deletePlaylist(playListIndex) {
    this.pListStorage.deletePlaylist(playListIndex);
  }
  /**
   * Edits certain playlist fields
   * @param playListIndex 
   * @param name 
   * @param description 
   */
  editPlaylist(playListIndex, name, description) {
    let playlistListModal = this.modalCtrl.create(EditPlaylistComponent, {playListIndex: playListIndex, name: name, description: description});
    playlistListModal.present();
  }
}
