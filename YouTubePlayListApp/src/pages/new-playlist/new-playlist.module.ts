import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewPlaylistPage } from './new-playlist';

@NgModule({
  declarations: [
    NewPlaylistPage,
  ],
  imports: [
    IonicPageModule.forChild(NewPlaylistPage),
  ],
})
export class NewPlaylistPageModule {}
