import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { PlaylistStorageProvider } from './../../providers/playlist-storage/playlist-storage';
import { Storage } from '@ionic/storage';
import { PlaylistsListComponent } from '../../components/playlists-list/playlists-list';
import { SearchYouTubePage } from '../search-you-tube/search-you-tube';

@IonicPage()
@Component({
  selector: 'page-new-playlist',
  templateUrl: 'new-playlist.html',
})
export class NewPlaylistPage {

  @ViewChild('name') playlistName;
  @ViewChild('description') playlistDescription;
  
  constructor(
    private storage: Storage,
    private pListStorage: PlaylistStorageProvider, 
    public navCtrl: NavController, 
    public navParams: NavParams) {
  }
  /**
   * Creates a new playlist from input fields
   */
  createNewPlaylist() {
    this.pListStorage.createNewPlaylist(
      this.playlistName.value, 
      this.playlistDescription.value
    );
    this.navCtrl.setRoot(SearchYouTubePage);
  }
}
