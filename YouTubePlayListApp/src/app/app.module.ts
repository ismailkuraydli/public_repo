import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { YoutubeProvider } from '../providers/youtube/youtube';

import { HttpModule } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';

import { SearchYouTubePage } from '../pages/search-you-tube/search-you-tube';
import { PlaylistsPage } from '../pages/playlists/playlists';
import { SingleplaylistPage } from '../pages/singleplaylist/singleplaylist';
import { VideodetailsPage } from '../pages/videodetails/videodetails';
import { NewPlaylistPage } from '../pages/new-playlist/new-playlist';
import { PlaylistStorageProvider } from '../providers/playlist-storage/playlist-storage';
import { PlaylistsListComponent } from '../components/playlists-list/playlists-list';
import { EditPlaylistComponent } from '../components/edit-playlist/edit-playlist';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    SearchYouTubePage,
    PlaylistsPage,
    SingleplaylistPage,
    VideodetailsPage,
    NewPlaylistPage,
    PlaylistsListComponent,
    EditPlaylistComponent
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    SearchYouTubePage,
    PlaylistsPage,
    SingleplaylistPage,
    VideodetailsPage,
    NewPlaylistPage,
    PlaylistsListComponent,
    EditPlaylistComponent
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    YoutubeProvider,
    HttpClient,
    HttpModule,
    PlaylistStorageProvider
  ]
})
export class AppModule {}
