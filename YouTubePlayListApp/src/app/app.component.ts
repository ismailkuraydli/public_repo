import { Component, ViewChild } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { MenuController } from 'ionic-angular/components/app/menu-controller';
import { SearchYouTubePage } from '../pages/search-you-tube/search-you-tube';
import { Nav } from 'ionic-angular';

import { Storage } from '@ionic/storage';
import { PlaylistsPage } from '../pages/playlists/playlists';
import { NewPlaylistPage } from '../pages/new-playlist/new-playlist';

@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  @ViewChild(Nav) nav: Nav;
  
  rootPage:any = HomePage;
  pages: Array<{title: string, component: any}>;

  constructor(
    platform: Platform,
    public menu: MenuController, 
    statusBar: StatusBar, 
    splashScreen: SplashScreen,
    public storage: Storage
  ) {    
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });

    this.pages = [
      {title: 'Home', component: HomePage },
      {title: 'Search', component: SearchYouTubePage},
      {title: 'Playlists', component: PlaylistsPage},
      {title: 'New Playlist', component: NewPlaylistPage}
    ];


  }

  openPage(page) {
    // close the menu when clicking a link from the menu
    this.menu.close();
    // navigate to the new page if it is not the current page
    this.nav.setRoot(page.component);
  }
}

