import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

/*
  Generated class for the YoutubeProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class YoutubeProvider {
  apiKey = 'AIzaSyDvtD4VxLomLpWhvsfDjwMrXwTMkyEOPrc';
  
  constructor(public http: Http) {
    console.log('Hello YoutubeProvider Provider');
  }

  getSearchResults(searchWord) {
    return this.http.get(
      'https://www.googleapis.com/youtube/v3/search?key=' + this.apiKey + 
      '&maxResults=25'+ 
      '&q=' + searchWord + 
      '&part=snippet')
      .map((res) => {
        return res.json()['items'];
      }
    )
  }
}
