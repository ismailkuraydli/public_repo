import { Storage } from '@ionic/storage';
import { Injectable } from '@angular/core';
import { Events } from 'ionic-angular/util/events';

@Injectable()
export class PlaylistStorageProvider {
  /**
   * Class constructor for dealing with storage and its CRUD operations
   * @param storage 
   * @param events 
   */
  constructor(public storage: Storage, public events: Events) {
    
    console.log('Hello PlaylistStorageProvider Provider');
  }
  /**
   * Gets data from storage and returns an array of playlist objects
   */
  storedPlaylists() {
    return this.storage.get('playlists').then((result) => {
      if (result) {
        return  JSON.parse(result);
      } else {
        return [];
      }
    })
  }
  /**
   * Gets all playlists
   */
  getPlaylists() {
    return this.storedPlaylists().then((result) => {
      return result;
    })
  }
  /**
   * Gets one playlist according to its index
   * @param playlistIndex 
   */
  getPlaylist(playlistIndex) {
    return this.storedPlaylists().then((result) => {
      return result[playlistIndex];
    })
  }
  /**
   * Gets all videos from a playlist according to its index
   * @param playlistIndex 
   */
  getPlaylistVideos(playlistIndex) {
    return this.storedPlaylists().then((result) => {
      if(result[playlistIndex]){
        return result[playlistIndex].videos;
      } else {
        return [];
      }
      
    });
  }
  /**
   * Creates new playlist and add it to storage
   * @param playlistName 
   * @param playlistDescription 
   */
  createNewPlaylist(playlistName, playlistDescription) {
    this.storedPlaylists().then((result) => {
      result.push({
        name: playlistName, 
        description: playlistDescription,
        videos: []
      });
      this.save(result);
    })
  }
  /**
   * Adds video to a playlist at a given index
   * @param playlistIndex 
   * @param video 
   */
  addVideoToPlaylist(playlistIndex, video) {
    this.storedPlaylists().then((result) => {
      result[playlistIndex].videos.push(video);
      this.save(result);
    })
  }
  /**
   * Saves playlists array of objects to storage and fires an event
   * @param result 
   */
  save(result) {
    this.storage.set('playlists', JSON.stringify(result)).then(()=>{
      this.events.publish('storage:changed', result);
    });
  }
  /**
   * Deletes a playlist at a given index
   * @param playlistIndex 
   */
  deletePlaylist(playlistIndex) {
    this.storedPlaylists().then((result) => {
      result.splice(playlistIndex, 1);
      this.save(result);
    })
  }
  /**
   * Deletes a video at a given index from a playlist at a given index
   * @param playlistIndex 
   * @param videoIndex 
   */
  deleteVideoFromPlaylist(playlistIndex, videoIndex) {
    this.storedPlaylists().then((result) => {
      result[playlistIndex].videos.splice(videoIndex, 1);
      this.save(result);
    })
  }
  /**
   * Edits playlist fields
   * @param playlistIndex 
   * @param playlistName 
   * @param playlistDescription 
   */
  editPlaylist(playlistIndex, playlistName, playlistDescription) {
    this.storedPlaylists().then((result) => {
      let vids = result[playlistIndex].videos;

      result[playlistIndex] = {
        name: playlistName, 
        description: playlistDescription,
        videos: vids
      };
      this.save(result);
    })
  }
}
