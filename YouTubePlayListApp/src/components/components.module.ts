import { NgModule } from '@angular/core';
import { PlaylistsListComponent } from './playlists-list/playlists-list';
import { EditPlaylistComponent } from './edit-playlist/edit-playlist';
@NgModule({
	declarations: [PlaylistsListComponent,
    EditPlaylistComponent],
	imports: [],
	exports: [PlaylistsListComponent,
    EditPlaylistComponent]
})
export class ComponentsModule {}
