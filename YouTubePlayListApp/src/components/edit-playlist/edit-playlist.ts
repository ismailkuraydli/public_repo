import { Component, ViewChild } from '@angular/core';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { PlaylistStorageProvider } from '../../providers/playlist-storage/playlist-storage';
import { NavController } from 'ionic-angular/navigation/nav-controller';

@Component({
  selector: 'edit-playlist',
  templateUrl: 'edit-playlist.html'
})
export class EditPlaylistComponent {

  playlist: Promise<any>;
  playlistIndex: any;
  
  @ViewChild('name') playlistName1;
  @ViewChild('description') playlistDescription1;

  playlistName: string;
  playlistDescription: string;
  
  constructor(
    public params: NavParams, 
    public pListStorage: PlaylistStorageProvider,
    public navCtrl: NavController
  ) {
    console.log('Hello EditPlaylistComponent Component');
    this.playlistIndex = this.params.get('playListIndex');
    this.playlist = this.pListStorage.getPlaylist(this.playlistIndex);
    this.playlist.then((result) => {
      this.playlistName = result.name;
      this.playlistDescription = result.description;
    })
  }
  /**
   * Edits selected playlist fileds
   */
  editPlaylist() {
    this.pListStorage.editPlaylist(
      this.playlistIndex, 
      this.playlistName1.value, 
      this.playlistDescription1.value
    );
    this.navCtrl.pop();
  }
}
