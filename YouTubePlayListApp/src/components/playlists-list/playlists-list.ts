import { Component } from '@angular/core';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { PlaylistStorageProvider } from '../../providers/playlist-storage/playlist-storage';
import { NavController } from 'ionic-angular/navigation/nav-controller';


@Component({
  selector: 'playlists-list',
  templateUrl: 'playlists-list.html'
})
export class PlaylistsListComponent {

  playlists: Promise<any[]>;
  video: any;

  constructor(
    params: NavParams, 
    public pListStorage: PlaylistStorageProvider,
    public navCtrl: NavController
  ) {
    console.log('Hello PlaylistsListComponent Component');
    this.video = params.get('video');
    this.playlists = this.pListStorage.getPlaylists();
  }
  /**
   * Adds selected video to the selected playlist
   * @param playlistIndex 
   */
  addVideoToPlaylists(playlistIndex) {
    this.pListStorage.addVideoToPlaylist(playlistIndex, this.video);
    this.navCtrl.pop();
  }
}
